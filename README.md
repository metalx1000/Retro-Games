
# Retro-Games

Copyright Kris Occhipinti 2023-09-02

(https://filmsbykris.com)

License GPLv3


# About
This is a collection of scripts that makes it easy to install retro video games on a Debian and based Linux system.
Please make sure that you have the legal rights to install these games before installing a play them.
Just because a game is in the list does not mean that it's free for everyone to play.
The purpose of these scripts is to make it easy for people who own these games to get them installed on modern systems.

# How to Use

Install primary script
```bash
sudo apt install wget fzf
sudo wget "https://gitlab.com/metalx1000/Retro-Games/-/raw/master/retro_games.sh" -O /usr/local/bin/retro_games
sudo chmod +x "/usr/local/bin/retro_games"
```

Most games executables will be placed in /usr/local/games,
So be sure to add it to your PATH variable
```bash
PATH=$PATH:/usr/local/games
```

Run the script
```bash
retro_games
```

You're presented with a list of games that you can filter and searched through.
Once a game is selected the needed files will be downloaded and installed.

# Notes about game play

```
For Nintendo Games Mednafen is used
for controller setup press <Alt>+<Shift>+<player number>
replace <player number> with players number (example: 1)
```
