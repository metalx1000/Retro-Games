#!/bin/bash
######################################################################
#Copyright (C) 2023  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
######################################################################

trap "exit 1" 10
PROC=$$

game_dir="/usr/local/games"

function error(){
  echo $* 1>&2
  #exit 1
  kill -10 $PROC
}

function check_game_dir(){
  [[ -d "$game_dir" ]] && return
  sudo mkdir -p "$game_dir"
  [[ -d "$game_dir" ]] || error "Unable to Create $game_dir"
  PATH="$PATH:$game_dir"
  echo "$game_dir has been created."
  echo "Please add $game_dir to your PATH variable."
  echo "PATH=$PATH:$game_dir"
  read -p "Press Enter to Continue." v
}

function select_game(){
  url="https://gitlab.com/metalx1000/Retro-Games/-/refs/master/logs_tree/scripts?format=json&offset=0&ref_type=heads"
  gamelist="$(wget "$url" -qO-|tr "{" "\n"|grep file_name|cut -d\" -f4|sort)"
  [[ $gamelist ]] || error "Unable to retrieve game list."

  game="$(echo "$gamelist"|fzf --prompt="Select a Game: ")"
  [[ $game ]] || error "No Game Selected"
  echo "$game"
}

function get_game(){
  [[ $1 ]] || error "No Game Given."
  game="$1"
  url="https://gitlab.com/metalx1000/Retro-Games/-/raw/master/scripts/$game"

  script="/tmp/retro_game_$(date +%s)$RANDOM"
  wget "$url" -O "$script"
  chmod +x "$script"
  "$script"
  rm "$script"
}

function play_installed_games(){
  game="$(list_installed_games)"
  [[ $game ]] || error "No Game Selected"
  $game
  exit
}

function list_installed_games(){
  find $game_dir -executable -type f|while read f;
do
  basename $f
done|fzf --prompt="Select a Game to Play: "
}

function help(){
  echo "This script will try to download and install games you select from a list.
  Please be sure you own the right to play games you select.

  Useage: $0

  Useage: $0 <option>

  Options:
  help - Display this help file
  list - list installed games"

  exit
}

[[ "$1" == "help" ]] && help
[[ "$1" == "list" ]] && play_installed_games
check_game_dir
get_game "$(select_game)"

